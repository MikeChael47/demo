<?php

use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// Product Conntroller
Route::get('/product', [ProductController::class, 'index'])->name('product');
Route::get('/product/{id}', [ProductController::class, 'show'])->where('id','[0-9]+');
Route::get('/product/{name}', [ProductController::class, 'show'])->where('name','[a-zA-Z]+');
Route::get('/product/about', [ProductController::class, 'about']);

// Page Controller
Route::get('/page',[PageController::class,'index'])->name('page');

// POST Controller
Route::get('/posts',[PostController::class,'index'])->name('posts');



