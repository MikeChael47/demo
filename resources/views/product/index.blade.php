<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @include('layout.header')

    @section('content')
    <!-- <h1>PRoduct PAge</h1> -->
    <p>Title : {{ $title }}</p>
    <p>Description : {{ $description }}</p>
    <i>List of product</i>
    <ul>
    <!-- Link to product page -->
    <a href="{{ route('product') }}">Link to product</a>



    <!-- print list item -->
    @foreach($list as $item)
        <li>{{ $item }}</li>
    @endforeach
    </ul>
</body>
</html>