<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        $title = 'Welcome to my laravel page, check your banana !';
        $description = "This is decription about the title, check it up!, and feel free to read ";
        $list = [
            'productOne' => 'Iphone',
            'productTwo' => 'SamSung'
        ];
        return view('product.index',compact('title','description','list'));
    }

    public function show($id)
    {
        $notify = "Your id is  : " . $id;
        echo $notify;
    }

    public function about(){
        return "<i> About This Page </i>";
    }
}
