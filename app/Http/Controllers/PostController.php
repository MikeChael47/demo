<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        // like
        $title = 'Welcome to my laravel POST page, check out your banana !';
        $description = "This is decription about the title, check it up!, and feel free to read ";
        $list = [
            'productOne' => 'Iphone',
            'productTwo' => 'SamSung'
        ];
        return view('posts.index',compact('title','description','list'));
    }
}
